This diary file is written by Chi Wei, Hsieh in the course Professional skills for engineering the third industrial revolution.
Chinese name: 謝濟緯
Student ID: I54091149

# 2021-09-23 #

* Still trying to figure out what git is about
* Since electric cars are gonna be a lot cheaper, I'm gonna start investing in Tesla right now

# 2021-09-30 #

* The ability to identify fake news is important
* Never blindly believe nor reject any given information
* The method used to require statistical data has a great impact on the validity and credibility of the results 
* Sources are important as they provide authenticity to data

# 2021-10-07 #

* Loans create money, that's just so cool, I wanna make money too
* Income inequality is terrible, the rich should help the poor more

# 2021-10-14 #

* People who lack the emotional support that they need might go on dark paths
* Ignorance leads to most conflicts
* We should know ourselves better, so that we don't get fooled by extremist propagandas

# 2021-10-21 #

* Doing exercise stimulates the brain to have better function
* Being aware of our physical status allows us to have better performance
* Me as an athlete since a young age can confirm that the above two statements are true in some ways

# 2021-10-28 #

* Many people didn't contribute to the supergroup project, sad
* This week's course is about depression. My ex had depression, I didn't know the right way to deal with that back then. (Plus we had a long time seperated and broke up)
* I like the thought that the way to help depressed people is not by bridging people together, but closing the gap
* Listening and caring is what most depressed and suicidal people need
* Many depressed people see being depressed as being week, hiding their problems from others
* Be yourself, whether you're depressed or not. There is no need to feel bad for who you are