This diary is written by Sara Erdenezul B24067033 in the course "professional skills for engineering the third industrial revolution"


## 2019-09-19

* my first class 
* Presentations on last week's topics 
* Is the world a better place? --Statistically it is 
* TA offered everyone good advice and comments
* Learned how to cite work in presentations 


## 2019-09-26

* Fake news
* Goes futher than meaningless facebook posts and can have real consequences
* Used as warfare
* Used as proaganda 
* How can we fight fake news? 
* Research your source 


## 2019-10-03

* Economy: why is it not taught to everyone?
* Bretton Woods System: countries agree to fixed currencies
* 3 main functions of money: Medium of exchange, a unit of account, &store of value
* How do banks work?
* Banks are supposed to work as intermediaries but don't 
* Better to break up the banks to small local banks --more accountable (may get bought out if they get too big)
* The cycle of banking
* Credit and Money 


## 2019-10-17

* Fascism 
* Fascists declare their country as the best - they make their people turn away from the rest of the world
* No ugly monster in the real world 
* Community, Identity, & Purpose 
* Democracy is based on human emotions 


## 2019-10-24

* Knowledge is well justified belief 
* Claim-test it out- true or false 
* Nothing is absolute
* Exercising will protect against incurable diseases. 
* 30% of surgical procedures were conducted without looking into nonsurgical procedures 
* Money incentives over whats best for the patient 
* A health fee 
* Public medical data to activally keep people healthy 


## 2019-10-31

* Most of my close friends have gone through depression, all on different levels
* I don't think people really know they're depressed
* How to handle your depressed friends 
		Be normal with your depressed friends
		Invite them out 
		Be open & talk 
* Would you know what to say to a suicidal person?
	Listen 
* Break the stigma of mental health. 
* It's not physical so it's easier to put off 
* Having feelings isn't a sign of weakness


## 2019-11-07

* Free will is easily manipulated 
* People don't really understand what they choose
* We can change our opinions
* There is pressure to stay consistent
* Know that you don't know yourself as much as you think you do
* 1. Belonging  2. Purpose  3. Transcendence 
* Storytelling- the story you tell about yourself


## 2019-11-14

* We have more power than we think we do 
* Legal fiction
* Ignorance is our fault, we neec better education of the law
* Even police don't know the law
* Freedoms are being taken away 

## 2019-11-21

* The feeling that we have to make an impact 
* Working up with lower self esteem
* Filtered life 
* Our dependence on social media and our phones It is an addiction 
* Widespread- affects many people ex. How they get a job 
* Mysterious - they aren’t aware they’re being scored 
* Destructive - algorithms that ruins ppls lives unfairly 

## 2019-11-28

* History is repeating itself 
* The government should look at history for guidance 
* Be smart in picking which news you choose

## 2019-12-05

* Learned about the planetary boundries 
* There is more than I thought 
* Some boundries are hard to define
* What can we do to help our planet? It doesn't have to be a big impressive plan, something small everyone can incorpurate into their lives. 

## 2019-12-12

* Planetary boundaries 
* Feels like everyone only knows about climate change
* Seems like everything is linked
* Social Issues 
* Next to planetary boundaries they don't seem that important but they shouldn't be over looked 
* Vote on what we can do to help our planet 


## 2019-12-19

* voting results from the previous class make me distrust voting 
* debated in class: capitalists, workers, and enviormentalists
* What habits should we develop in order to lead a sucessful life. 
## 2019-12-20 Friday
* unsuccessful and unproductive
* Worked on a group project but it was pretty useless 
* Had to ride home in the rain 
* Didn't have time to study because of work
* WIll try to wake up tomorrow 
## 2019-12-20 Saturday
* Successful and productive
* I woke up early and worked on a group project 
* We were able to film and finish our video portion
* Will go buy a desk tomorrow
## 2019-12-20 Sunday
* Successful and productive
* Put together a desk 
* Studied for class 
* But I was homesick 
## 2019-12-20 Monday
* Succesful and productive
* Finalized one of the presentations
* Worried about Christmas gifts  
## 2019-12-20 Tuesday
* Successful and productive
* Final Project Presentation 
* Went Christmas shopping, made me excited  
* Will finish making gifts tomorrow 
## 2019-12-20 Wednesday
* Successful and productive
* Another final presentation
* Finished making gifts 
* Went to Christmas dinner

5 Rules 
1. If the task feels too big, tell yourself you'll only do 5 mintues of it  
2. Wake up early, even on the weekends
3. Priortize your tasks 
4. Reduce Distractions - limit apps 
5. If it can be done now, do it now. 

## 2019-12-26

* Group presentations
* How do we create solutions?

## 2020-01-02

* Why do things go viral?
* Social media means people are connected and use it for communication 
* Connection & realitiblity, humor, and you can share it in tour community (challenges) 
* We must connect education to the real world so students care
* If what students learn can't be applied to their world, what good can it do and why should they care?
* Students are ready to learn about social justice and it can help them learn and get enganged 

## 2020-01-09
* Can finding a soulmate be dwindled out to math? 
* Love is full of patterns, and math is the study of patterns 
* Even online dating sites are built up mathematically 
* Apparently its better to be either a big hit or a big miss
* Its better to play up what makes you different
* Best way to pick a marriage partner: 
* Reject everyone at first, then marry the next person that seems to be the best. 
* Positivity keeps divorce away 
* Marriage is continuous work
* Why is heartbreak so painful? 
* Heartbreak can make you irrational 
* Heartbreak can be like addiction, getting hooked on looking for an explanation or recalling memories and obsessing over your ex , these things are the equivalent to getting a hit of addictive drugs. 
* False nostalgia - you think things were better than they were 
* Heartbreak presents the same emotions as going through a loss/death of someone 
* Remember to support and have patient with people going through a heartbreak