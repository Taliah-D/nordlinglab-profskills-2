# 2021/09/30 #
- I found the topic for the second lecture very important, because it helps us to recognize fake news from real news.
- I liked each group's focus for the question Is the world getting better or worse, I think they were all different and interesting.
- I really like to work with my teammates, they're all very responsible, and we all contribute to do the assignments. 

# 2021/10/07
- In general, economics is not a topic a enjoy to discuss, because is so complex. But, for this class, the elements shown helped me to understand a little better the way it fluctuates and the impact of that fluctuation in our life.
- I already had Finances classes back in High School, and thanks to that I wasn't completely lost, buy the're so many terms I wasn't familiar with and with this class I became more related to them.
- I wasn't aware of the economical situation here in Taiwan, I found it really interesting.

# 2021/10/14 #
- Today's Ted Talks were my favorites until now. 
- I didn't really understand what fascisim was about until I saw today's video about it. It was simple but pretty accurate. Actually, the thing that man said about the democracy and how it works, made me analyze the parameters I consider when it comes to select a candidate, and that I should start to think more rationally about those kind of topics, specially during the election times.
- The second Ted Talk was the one that moved me the most. I found myslef so overwhelmed by the things that man said, and by the way he saw and now sees people and situations. For me, he just proved that people can change for good.
- I would really like to mention that when he said "Go out there, and find the person that least deserves your compassion and give it to them, because they're who need it the most" it made me cry. It completely change my perspective about people who may make mistakes in life and realize it, but don't have a chance to prove them wrong because nobody thinks they worth it. It reminds me to a phrase from a series, where an inmate asks to the guard why does she treat them so nicely, and not with violence like the other guards. And the guard says that is because the only difference between them is that the inmates were caught for their mistakes in life.

# 2021/10/21 #
- I've been always aware that a good state of mind has a lot to do with the physical state of our body, the instinct to cover our physical necessities (as it is exposed in Maslow's pyramid), but I never understood the process from the physical to the mental state until I saw today's video about it. I think is something I would like to apply in my everyday life.
- I'm used to do regular exercise since my childhood (I'm a ballerina since I'm 10 years old) and it is because of that I never really noticed the good effects on doing exercise until now, that I don't have the same amount of physical exercise per week since I started College. The days were I do exercise from the days I don't definitley hit different and now I can totally perceive this difference as for my mood, my appetite, my focus, sleep quiality etc., suddenly improve the second after I finish exercising. 

# 2021/10/28 #
- 
