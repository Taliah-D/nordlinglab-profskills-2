This diary file is written by Hugh Chen E14086282 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23
* I am still learning how to use this GIT tool. Seems to be easy, but quite difficult actually.

# 2021-09-30
* I've been injecting the vaccine for the covid-19, making me so uncomfortable to pay attention in class.
* It's a pity to barely learn anything because of the situation, but I really can't control.

# 2021-10-07
* Introducing about financial system, deposits and loans.
* Gold is considered to be the ultimate safe haven.
* When a bank's deposit-to-loan ratio is greater than 1, the bank has loaned out every cent of its deposit.
* "Fungible"
* The purchasing power of each currency has dropped exponentially for the last hundred year.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.

# 2021-10-14
* Fascismo-法西斯主義
* "Extremist"
* Human are the most imaginative.

# 2021-10-21
* How you think affects how you do. How you feel affects how you think. Your emotions affect how you feel. Your physiology affects your emotions.
* Seems to be easy to control people if learn enough about psychology or the brain.
* Exercising improves your brain.
* Exercise 3~4 times per week at least, with 30 minutes every time.
* True health care system could save us from unnecessary costs and risky procedures.

# 2021-10-28
* We were assigned into a super group in class. I can strongly feel the differeance between a small group of three people and a big one with more than ten. The progress of discussing is much slower although we can hear more different opinions from others.
* From a presentation from another group, I learned the importance of staying away from cellphone before going to bed and sleeping enough. The former leads to a better sleep and the latter benefits my body in many ways.
