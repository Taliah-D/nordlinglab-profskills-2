*This diary file is written by Sam Sheu E14051180


# Week3 180927

* It was my first time in this class. One of my friends recommended that I should enroll in this class since it give us a good opportunity to practice English,
having a deep discussion about week’s topic, furthermore, make new friends from another department to broaden our vision. It is exactly right.
* Today we discussed about the fact news and how do we recognize it. It’s an important topic since the proliferation of platforms upon which 
we get our information make us easy to access to the news, but also including the fake news. 
* From the video, there are some tips we can keep in mind: First, be sensitive about the data. Second, think about the meaning behind the news or statistics. 
Third, don’t totally believe (or discard) the statistics, being more open-minded. 
* I was so sorry that I attended the class in the last hour. Next time I would search about the course earlier and enrolled it in the first two weeks. 


# Week4 181004

* It was my first time to give a presentation in today’s class. I made a big mistake that I didn’t fully discuss with my partner, resulting in having no claim, 
typing in a wrong form and forget to give a reference.
* If we have some questions about the information, try to type the key word on the internet and you can find out something interesting. 
* If trying to prove a claim which is based on the statistics, it is inappropriate to use the data again while proving it. 


# Week5 181011

* We cannot use question mark on the title. 
* The mount of your deposit is just a digit, banks take it, use it, and thus create money. 
* Why do we need to know about extremism? What is its relation for being an engineer? That is because if we don’t know about this, we keep silence, and silence leads to the world war 2.
* Having a strong sense of nationalism would be a good, powerful country. 
* Nowadays, holding data is important. It has replaced both land and machines as the most important asset.
* Democracy was better at possessing data and making decisions

# Week6 181018

* There is a good concept in the video we saw today: sick-care system and health-care system. 
Comparing with each other, health-care system is a true action to maintain our health: doctors get paid only if people are healthy,
however, sick-care system is just a way to help patients get through the sick: doctors get paid when they cure the illness.


# Week7 181025

* It was my second time to present. I choose the claim that speaker had said: What if we paid doctors to keep people healthy. 
In the beginning, I want to distinguish the difference between sick-care and health-care, so I give the data of GDP, HAQ and what country use 
what system which is more related to the “health-care” system. But there is no truly “health-care” system in the world, 
and the system we use cannot categorize into two of them. So the process of my proving isn’t very rigorous. I should notice this before I proved it. 
* People who suffer from depression often feel that they are sorry for their depression, but sometimes, accept it and confront at it is a better way to fight against it. 


# Week8 181101

* Four pillars of a meaningful life: belonging, purpose, transcendence and storytelling. 
* Happiness comes and goes. But when life is really good, and when things are really bad, having meaning gives you something to hold on to. 
* It’s interesting, what if I say: The meaning of I living in the world is to become happy, what am I going to be?
* Chasing happiness can make people unhappy.
* Our claim: Violence is a learned behavior. If you accept this as a truism, nonviolence can also be a learned behavior, but you have to teach it, because kids are not going to learn that through osmosis.”

# Week9 181108

* It's a little bit difficult for me to watch a video in English without subtitle, but it also remind me the reason why I should make my English better: I can hear more, learn more, and chat more. 
* This week's workshop is to investigate some legal concept and how they were implemented in legislation in TW & U.K.. Our team's topic is to compare the terms of "legal" and "lawful".  
* I wish we can have more and deep discussion in class, not just watching videos and let the fime fly by. 

# Week10 181115

* For the presentation of homework, I compare the term "Legal" and "Lawful".

	1. Lawful : It's allowed or recognized by law.
				It conform to the substance of law [its content].
	2. Legal : it's connected with the law.
				It alludes to the form of law [its form or appearance].

* To sum up, "Lawful speaks of freedom while legal speaks of being bound." 
The term lawful more clearly suggests an ethical content than does the word legal.

# Week11 181122

* My presentation was sucked. I didn't prepare very well, but meanwhile, I didn't think my teammate want to discuss our workshop. 
* How to make sense of events?
	1. We have to view the news from different aspects since there is no non-bias news on the world. 

# Week12 181129

* I was in the class at the first 1 hour and later leave for statutory reasons. I'm really sorry for my absense. Next time, if I could, I'll make sure that the discussion of SAE will not arrange during the class. 

# Week13 181206

* Our workshop task is to find the meaning of Planetary Boundary. And what is the Ocean Acidification? And what is it so important?
Since our hmans are inextricably linked to the health of the ocean, and we have always relied on the ocean's resources for food, recreation, transportation and medicines. 
We have to face up to these problems, including "Climate Change", "Novel Entities", "Stratospheric ozone depletion", "Atmospheric Aerosol Loading", 
"Biochemical Flow", "Freshwater Use", "Land-system Change", "Biosphere Integrity". 

# Week14 181213

* Analyse why you felt successful/unsuccessful and productive/unproductive. 

	1. For me, productive is much simpler to explain. If I do lots of work like studying, solving problem, having a discussion, then I'll say that day is productive.
	On the contrary, if I do nothing, or I do something but not using my brain, I would say it is unproductive.
	2. The difficulty is to define success. For our human being, we call someone who earn lots of money a successful person, or someone who is famous or having a good reputation, we describe that person as successful.
	In my opinion, having a fortune or having a good reputation is that people might treat you more respectfully, and this leads to your happiness. What if we are alrealy being a happy guy, do we need fortune or reputation to let us happy?
	
	So maybe what we need for success is simple:
	(my opinion)
	productive = finish lots of work (something meaningful);
	successful = productive + happiness (feel fulfilled);
	
* What I felt in this week, and the selected day I want to share in this diary.
	
	1. On Saturday, I went back to home to see my parents and especially my elder sister whom I haven't seen for about three years.
We had a good time having a lunch, taking care of the plants together and playing Mahjong after the dinner. It was such a wonderful day 
eventhough my productive is 0.
	2. On Sunday, I spent the whole day studying Spanish. I like to study Spanish and feel that time flies by. It's a productive day and successful day. 

* Five rules to make me feel successful and productive. 
	
	1. Make a to-do list at the start of a day.
	2. Right on the bed before 11:50 pm.
	3. Do some review before each class eventhough I only have 10 minutes. Just do it. 
	4. Before a discussion, go on the internet to see if there are some information about it. 
	5. Don't be a stingy person, give some free time in the schedule to have a rest. 
	
# Week15 181220

* 2018-12-21
	1. A normal day, one class in the morning and one class in the afternoon, after that, have a meeting for the SAE competition and then play table tennis from 7:00 pm to 12:00 pm.
It was my second time doing exercise in that week, not very bad. Neither did I feel productive nor successful, however, not an awful day.

* 2018-12-22
	1. Do EV3 project all the afternoon and the night, and finally achieved our short-term goal. I felt productive but unseccessful since it took me too much time to do that project.
One remarkable thing is that ourself cook the dinner together and had a lot of fun. 

* 2018-12-23
	1. I didn't wake up until 2:00 pm. It seemed that I wasted my time on sleeping, but after I waked up, I made a to-do list. It really enhanced my efficiency doing the work, I felt productive and successful before another day is coming since I do lots of work in a short time. 

* 2018-12-24
	1. Only sleep for 4.5 hours and then wake up to prepare a quiz. In the night, having a discuss on the material of our SAE electrical vehical.
Since we have some conclusion at the end of the meeting, I felt a little bit productive and successful. 

* 2018-12-25
	1. Having a busy day. Wake up at 10:00, 10 to 12 for Automatic Controll, 13 to 15 for Spanish and 15 to 17 for General Education.
After the dinner, meeting from 19 to 21, and then 21 to 22 playing table tennis. A normal day. 

* 2018-12-25
	1. Today, I study for about 3 hours and do EV3 project for about 4 hours. We achieved our midterm goal and very exciting.  I thing it's a productive and successful day. But a fly in the ointment is that I go to bed too late, it leads to me feeling drowsy. 
	
# Week16 181227
* When I do the homework, I really don't know what unitychain exactly is. But after their introduction, now I have a brief concept of what they are doing.
 In addition, there are some advantage with using unitychain. First, it removes 3rd parties, that means it may be safer and simpler. 
Second, value is now available at the protocol layer. Third, it's decentralization. 
* I love that day's class since it's like brainstorming, with someone else attendence, we can come up with some great idea. 

# Week17 190103
* Maybe I should try online dating app.(lol) Otherwise, I only know people from our department and it's very boring. 

# Week18 190110
* We had a Final Exam at the first 20 minutes. I think I didn't prepare very well since the grade of the exam is not very good. 


