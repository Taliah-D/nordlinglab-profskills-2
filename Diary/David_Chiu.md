﻿This diary file is written by David Chiu F04057011


# 180913 Week 1

* Exponential growth will happen sooner then what we think.
* Technology will get even cheaper
* Advances in technology will create new jobs positions.

# 180920 Week 2

* War stats are different from the TED talk.
* The world is getting better and better with time.

# 180927 Week 3

* Average income in a country is not accurate to represent the real average income of the population.
* Nowadays a lot of people create fake news to deceive others.

# 181004 Week 4

* Concentration of the banking section is not good since all the money would be concentrated.

* How the bank get the money is through borrowing money from population.
* Before asking the bank for a loan you have to make sure the money is invested in something that can help you generate the money to payback the loan.
* Transactions usually consist of a buyer and a seller.

# 181011 Week 5

* If all the people go to bank the same day at the same moment bank can bankrupt.
* Hatred is born from ignorance.
* Country that are not tend to nationalism is violent and poor.
* If you are engineer, we should find a way to prevent concentration of data to fall in few people hands.
* Know our own weakness helps to avoids the trap of fascism.

# 181018 Week 6
* The brain is the most complex structure known.
* Physical activities can have powerful effects in the brain.
* Exercise can protects your brain against neurodegenerative diseases such as Alzheimer's or dimentia.
* Neurotransmitters are increased after workout.
* We should change the healthcare system in the way we should prevent people from getting sick and not treating people after they get sick.

# 181025 Week 7
* A single workout can help improve your reaction time.
* Depression is the leading cause of ill health and disability.
* Never take negative responses personally.
* If a person is depressed let them contribute with something in your life.
* Most of the people that want to commit suicide just need more atention from other people.
* Just staying quiet and listen to someone with depression would help them a lot.
* Never hesitate to ask for help if you need it, there's always someone willing to help you.

# 181101 Week 8 
* Most of the people that are depressed are seeking for attention.
* Humans have many defining moments in their own live.
* People must learn how to focus there anger in a healthy way.
* People that have a meaning in life are more resilient, do better in school and work and can live longer.
* Everyone have a purpose in there life so it can be more meaningful.

# 181108 Week 9
* Our brain can be easily deceived.
* A person with high income cannot be related if a person is happy or not.
* When you buy a property in UK is not actually own by the buyer instead you are just a keeper.
* Money can store value or a purchase power, it can be also a medium of exchange, and a unit of accounting.
* UK have an act of parliament, while US have an act of congress.
* It is important to know ones own rights so we can prevent other people from taking advantange of you.


# 181115 Week 10
* People would like to have a job that can make an impact to society.
* A lot of parents give everything that there child wants and that's why they failed in there parenting job.
* Failed in parenting children would have a lower self esteem in the real world compare to others.
* Young generation really need attention from others since social media makes them feel important.
* When the young generation received a text message there brain produces dopamine that makes them feel happy.
* Everything in excess is bad for example you can have alcohol in moderation but not in excess.
* Jobs satisfaction and relationship can�t be attain instantaneous.


# 181122 Week 11
* We produce dopamine everything we look at our phone.
* Millennials people they are not necessarily narcissistic.
* The more you are into the social networking, the higher the risk to have depression.
* To find a good source of the news you should look for the original news without any filters.

# 181129 Week 12
* Newspapers headlines write what benefit them the most.
* Planetary risks that we are facing help us to be more transformative, such as for innovation for new ideas.
* Population growth has damaged the Earth.
* The rich minority has caused more damaged than others.
* Climate change is another factor that has damaged the Earth, since we are destabilizing the Antarctic Ice Sheet with the rising of sea level.
* The human being are the predominant driver of change at the planet.

# 181206 Week 13
* We have in total nine planetary boundaries.
* Each of the planetary boundaries are important and I dont think there is one more important than another one, since one can affect the others.
* Some of the planetary boundaries already exceeded the uncertainty level.
* We should be more concern in the problems we are facing since we only have one planet.

# 181213 Week 14
* Fossil fuels are very limited and there will be one day that it will be used up completely.
* Instead of burning fossil fuels we should find a greener way of producing energy.
* I believe the future will be of automated technology.
* Human beings should not live to work only instead to enjoy everyday as if it was the last day.
* Failure and loss are necessary for success.
* Don’t let failure stop you from success, because failure is just a stepping stone to make us successful.

* 1.On Monday I went to a football competition game, because I got injured two months ago with an shoulder dislocation I wasn’t able to exercise or do any heavy workout, I felt it was a successful day because after 2 months of recovery and therapy I could finally participate and win a game.
* Tuesday was a usual day of class which consist of waking up at 9 and finish at 6pm and could have my experiments course since they always have equipment problems, not a productive day at all.
* Wednesday after a normal day class, which I have the same cycle as usual, at evening we could solve some current problems in my capstone project.

* Reduce the usage of smartphones or laptops.
* Get started. Start with things you don’t like first, and don’t let laziness conquer you.
* Make a week plan, prioritizing tasks so you know what’s more urgent than others.
* Always take breaks because your brain needs it and it can also helps you reflect on what you are doing.
* Accept advises from others even though you don’t like to hear them.

# 181220 Week 15
* 12/21 Woke up at 8 am and after went back to sleep again because I forgot my class was at 10am, after I finished my class my group and I started working in our capstone project and made some advances. So I think was a productive day.

* 12/22 Woke up around 12pm and had my lunch with friends and went for some football and finished my essay and prepare my final presentation for other courses. Productive day

* 12/23 Woke up at 10am because I need to have a good breakfast and prepare my stuffs to go to Chiayi for my football game. After my football game I came back to take a nap because I was really tired and after got my dinner with some friends. At last I finished my final presentation for my general course. Productive day.

* 12/24 Woke up at 10 am because I need to buy a gift to play dirty Santa with my friends, and we had a dinner at a restaurant to celebrate Christmas Eve. Had a final presentation in my general course at 1pm, so after class I went in search of the gift because I couldn’t find the gift int the morning. Our dinner was at 7:30pm and after dinner we went to a bar to have some drink and do our gift exchange. Productive

* 12/25 Today I woke up at 7 am to go to Chiayi to go a tournament, the tournament was really tired since I didn’t had enough sleep and after the game I went to sleep. Unproductive

* 12/26 Woke up at 10 am since I need to finish a report for my Capstone project, we prepared everything such as the Matlab code and finalize the product as well. Productive

# 181227 Week 16
* Things go viral when the public is participating in something that makes them happy.
* People are connected in media, because can communicate between each other.
* School are important so children can learn skills to change the world
* Problem solving writing, collaboration, perseverance, and historical context can help in social justice.





# 190103 Week 17
* You can win in online dating by making people to think you are ugly it may help in your advantage, because sometimes people that are really attractive you think she will rejects most of the people. 
* People are afraid to be rejected so sometimes they don't even bother in trying.
* Try to avoid divorce when searching fro your partner.
* Online dating apps have created a wider chance of finding your othe half.
* If you have a broken heart, don't ignore it, accepting the problem is the first step to overcome it.

## 190103 Thu
1. Successful and productive
2. I woke at 8:30am and was getting ready for my final presentation and everything went smoothly just as planned. After that I started to work in my Matlab code for my Capstone final presentation.
3. For next I would try to start earlier so I don't need to rush at the very end.

## 190104 Fri
1. Successful and unproductive
2. Woke up at 9 am to get ready for my class at 10, after finishing class went for lunch with my friends. After lunch I need to get back to university because we have a presentation in our Capstone class, after presentation I went straight back to bed becasue last night I stayed but till 5am.
3. Should schedule better my task so I don't stayed up late.

## 190105 Sat
1. Unsuccessful and unproductive
2. Woke up at 1pm because I was still tired becasue I stayed up last night so went for my first meal of the day and after went back to watch some series.
3. Find more productive things to do and try to sleep early.

## 190106 Sun
1. Successful and productive
2. Woke up at 9 am and had my breakfast early and after went back to my apartment and started to finish my final project for Introduction to computer course. After finished that went to have dinner and after dinner I started with my final presentation in the course of Cultural Heritage.
3. Start my prepartation for final week ahead of the final week.


## 190107 Mon

1. Unsuccessful and unproductive
2. Woke at 12pm because I dont have morning class, and just had lunch and went to the OIA to ask them about the working permit procedure. Went back home and started to study.
3. Should work on something more since I have a lot of reports to do.

## 190108 Tue
1. Successful and productive
2. Woke up at 8am because I have a final test in Introdcution to computers and didn't want to be late, after the test finished I went to fill in the form for the application of the working permit and handle in the same day. After went to class and after dinner I started doing the dinal presentation for my other course and at last we finished around 4am which was not really smart.
3. Should prepare my stuffs more ahead so I don't need to stay overnight.

## 190109 Wed
1. Successful and productive
2. Woke up at 8am as well even though I didn't slept even becasue my class was at 9am and I didn't wanted to be late, the presentation went really good and later on I had a final test in combustion ocurse as well and I think it went well. After arriving home it was time to work in my other courses reports since I don't want to
   write report in a Friday.
3. Try to at least sleep 6-8 hours haha.


# 190109 Week 18
* Metadata is very important and dangerous at the same time, becasue it can track a person from any part ofthe world.
* Nowadays anyone can track a person with just a simple device call IMSI catcher.
* China is leading in the world of artificial intelligence.
* In my personal opinion I wouldn't like to live in China becasue there is no chance to commit any mistakes at all, and mistake are part of live.



