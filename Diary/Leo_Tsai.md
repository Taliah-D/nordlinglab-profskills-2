This diary file is written by Leo Tsai E14083137 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* This course operates in a whole different mannar than moodle, so it'll take some time for me to keep up the pace.
* I really learnt a lot from the exponential growth topic last week, hoping I can develop valuable skills along the course.
* I think exponential growth in green energy will lead to a world of abundance and everlasting.
* Glasl conflict escalation model subdivides human interaction into multiple stages, which is a novel idea to me.
* Scrolling on google to find out whiat GIT is, and found out Linus stated that git can be interpreted as anything, depends on your mood.


# 2021-09-30 #

* I find this course not as stressful as other courses, and we can also cultivate invaluable skills rather than academic knowledge in the mean time.
* A good statistic can be visualiaed with vivid diagram or gragh rather than cold numb data.
* In statistics, scale is everything. Once you change the scale(axes), you can change the story.
* I learned how to cite articles correctly today, which is definitely useful in all directions.
* Newsvoice topic: How to be free from mental conditioning caused by society
* Humans copy what other people are doing and it happens through repetitive content.
* The average person thinks that they get married at a certain age.
* Take a nine-to-five job instead of starting one's own buissness.
* The subconscious manufactures your behavior through analyzing the repetitive content in your environment.
* The solution is to hack your subconcious mind and exchange bad habits with healthy one.
* It's mainly a repetition fight.
* Cited from https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/


# 2021-10-07 #

* Professor talked about financial system today, and introduce some interesting facts about deposits and loans.
* Gold is often one of the assets that is considered to be the ultimate safe haven, which value usually doesn't fluctuate dramatically.
* If one bank's deposit-to-loan ratio is greater than 1, which implies that the bank has loaned out every cent of its deposit.
* It is the danger zone because it has no reserves to pay customers for demand deposits.
* It's interesting to learn that whenever Chinese's new year has come, the amount of money government held increased drastically
* due to the Chinese's envelope custom.
* I also learned a tricky word "fungible", which is something may be replaced by another equal part or quantity.
* Money is the typical instance that is fungible.
* The purchasing power of each currency has droped exponentially for the last hundred year.
* The inflation of currency has become so severe that I can't help wonder what can we afford to buy
* with a thousand dollars in the next or two decads to come, perhapes not even a drink.
* If the government keep letting the inflation sabotages our currency purchasing power, then we must publish a new currency.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.
* Every politician tells that we must need to fix this gap, and makes promises to their voters.
* However, who really commited to their promises after elected, who ever really has taken this issue seriously.
* Tragedies will still happen, and the gap will grow faster and faster until the poor can't withstand the extremely uneven 
* distrubution of resources.

# 2021-10-14 #
* I gave a presentaion today, and recieve a lot of advice from professor.
* I used to think that giving presentation in English is quite intimidating, however now I think that only be overly prepared can make my nerve relax. 
* Facism is used as a kind of general purpose abuse or they confuse facism with nationalism.
* Facism tells me that my nation is supreme and that I have exclusive obligations towards it.
* Now data is replacing both land and machines as the most important asset.
* Facism is what happens when people try to ignore the complications and to make life too easy for themselves.

# 2021-10-21 #

* Today's vedios are very interesting, and energetic. Explaning how people can be brilliant and toward a better version of ourselves in scientific way.
* Often people think that changing behavior will change the result, howevert it's not the case.
* There's 4 levels beneath behavior, which are thinking, feelings, emotions, and physiology from top to buttom.
* The most enchanting part is Dr.Alan explains that emotion and feelings are completely different things.
* "Emotion" is energy in motion, and those energy is come from physiology, including all the signals transmitted by our senses.
* In conclusion, emotion is all the signals inclusive of electric signal, pressure wave, and son on.
* feeling is the awareness of those energy flow in our body.
* In order to be brillient everyday, we must start to control those levels beneath behavior.
* Exercise is truly the key to a healthy life, working out consistently makes our brain release adequate amount of endorphins
* Endorphins can not only reduce the perception of pain, it also triggers a positive feeling in your body, such as boosting mood.
* I build a habit of exercising regularly in the past 3 years, it was hard to embrace the soreness accumulated in muscle at first.
* however, as endorphins began to release, I started to enjoy the process over pain.
* After all the hard work I put in, I realized that my body begin to shape, and become more fit than ever, which bring me sense of achievement.
* Now, I'm more confident, and proud of being recognized as a disciplined person, moreover, an atheletic person.



