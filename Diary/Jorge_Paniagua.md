This diary file is written by Jorge Paniagua E24097023 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I am learning step by step how to use this new tool I dind't use before called Bitbucket.
* From the first group presentation I realized that almost anything in the world use batteries and this devices may provably become the main energy souces for everything in the world.
* For the future, it seems to be unlogical to invest in metro systems anymore, since using autonomous vehicles is getting cheaper and more convenient.
* I learned about the international standard for dates and time - ISO 8601.


# 2021-09-30 #

* Most of the time to have a better understanding of some statistic results you need to understand the relationship between the one who did it and the analized data.
* You need to ask yourself if you're related to the data.
* Take more in count public statistics over private statistics.
* Fake news may be more dangerous than real weapons.


# 2021-10-07 #

* Consider the possibility that history could be changed and manipulated through fake news.
* A certain part of Taiwan's growth is apparently thanks to loans.
* The world economy has a great dependence on the dollar.


# 2021-10-14 #

* If a company knows you much more than yourself, they can control and manipulate your own deepest emotions and desires without even realizing it.
* You should never underestimate human stupidity.  It is one of the most powerful forces shaping history.
* NTD purchasing power today is much lower than its purchasing power 30 years ago.


# 2021-10-21 #

* Exercise is good for improving memory.
* You cannot assume that a claim is true just because it cannot be proven that it is false yet and vice versa.
* I am impressed by the percentage of people who are worried, stressed and anxious about climate change.
* I think I already got used to using GIT repository, it already catched my attention.
* I may make some researchs in the future about more fuctions of Bitbucket in order to use the most of it. 

# 2021-10-28 #
* Depression is a very complex problem to take it easy, the best way to deal with it is by looking for professionals help.
* Some people may think that talking to people with depression is easy, but the truth is that any wrong word or sentence could trigger results that make the situation turn even worse.
* When someone(with possible depression) wants to share their emotions with you, just try to listen and understand them if you are not capable enough to continue the conversation.
* After this lecture I took some time researching about this topic and reading some articles and experiences of some people who went through situations related to it, I think it is necessary for people wants to help their friends but don't know how to help them.
* It is interesting to analyze the fact that a lot of chemical reactions can cause certain kinds of feelings in living beings. This topic led me to question myself about how we are affected by things we don't even knoe about. Also, assuming the fact that we are made up of atoms, which are supposed to be lifeless, but the set of a few atoms creates cells, molecules, organs, and human beings, which have life. How is it that a set of lifeless things can create life?